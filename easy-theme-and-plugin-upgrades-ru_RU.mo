��          �       <      <  V   =  U   �  d   �  c   O  /   �  .   �  
          >   <  =   {     �     �  P   �     A     ]  C   x     �  �  �  v   �  v   A  �   �  �   g  Z   	  ^   q	  
   �	     �	  c   �	  c   ^
  :   �
  :   �
  �   8  &   �  *   �  C   &     j   A backup zip file of the old plugin version can be downloaded <a href="%1$s">here</a>. A backup zip file of the old theme version can be downloaded <a href="%1$s">here</a>. A plugin backup can not be created as creation of the zip file failed with the following error: %1$s A theme backup can not be created as creation of the zip file failed with the following error: %1$s Backing up the old version of the plugin&#8230; Backing up the old version of the theme&#8230; Chris Jean Easy Theme and Plugin Upgrades Moving the old version of the plugin to a new directory&#8230; Moving the old version of the theme to a new directory&#8230; Plugin Backup - %1$s - %2$s Theme Backup - %1$s - %2$s Upgrade themes and plugins using a zip file without having to remove them first. Upgrading the plugin&#8230; Upgrading the theme&#8230; http://wordpress.org/extend/plugins/easy-theme-and-plugin-upgrades/ https://chrisjean.com/ Project-Id-Version: Easy Theme and Plugin Upgrades
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-28 21:46+0000
PO-Revision-Date: 2018-03-06 11:09+0000
Last-Translator: Роман Вознюк <admin@mvdent.local>
Language-Team: Russian
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ Скачайте резервную копию старой версии плагина <a href="%1$s">здесь</a>. Скачайте резервную копию старой версии шаблона <a href="%1$s">здесь</a>. Невозможно создать резервную копию плагина, так как создание zip-файла завершилось с ошибкой: %1$s Невозможно создать резервную копию шаблона, так как создание zip-файла завершилось с ошибкой: %1$s Создание резервной копии старой версии плагина... Создание резервной копии старой версии шаблона&#8230; Chris Jean Easy Theme and Plugin Upgrades Перемещение старой версии плагина в новый каталог&#8230; Перемещение старой версии шаблона в новый каталог&#8230; Резервная копия плагина - %1$s - %2$s Резервная копия шаблона - %1$s - %2$s Обновляйте темы и плагины с помощью zip-архива, без удаления уже установленных версий. Обновление плагина... Обновление шаблона&#8230; http://wordpress.org/extend/plugins/easy-theme-and-plugin-upgrades/ https://chrisjean.com/ 